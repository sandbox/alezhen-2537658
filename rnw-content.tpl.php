<div id="rnw-pager">
  <table>
    <tbody>
    <tr>
      <th><?php echo t('Customer Information') ?></th>
      <th><?php echo t('Ordering Information') ?></th>
    </tr>
    </tbody>
    <tr>
      <td>
        <p><?php echo t('Full Name') . ': <strong>' . $items['name']; ?></strong></p>
        <p><?php echo t('Phone') . ': <strong>' . $items['phone']; ?></strong></p>
        <p><?php echo t('E-mail') . ': <strong>' . $items['mail']; ?></strong></p>
      </td>
      <td>
        <?php echo t('Created') . ': <strong>' . date('d.m.Y', $items['created_at']); ?></strong><br>
        <?php echo t('Type') . ': '; ?><strong>
          <?php
          if ($items['type'] == 1) {
            echo t('announcement');
          }
          elseif ($items['type'] == 2) {
            echo t('felicitation');
          }
          elseif ($items['type'] == 3) {
            echo t('vip felicitation');
          }
          ?>
        </strong>
        <br>
        <?php echo t('Air date') . ': <strong>' . date('d.m.Y', $items['dateair']); ?></strong><br>
        <?php
        if ($items['type'] == 2) {
          echo t('Airtime') . ': <strong>';
          if ($items['airtime'] == 1) {
            echo t('daily air');
          }
          elseif ($items['airtime'] == 2) {
            echo t('evening air');
          }
          elseif ($items['airtime'] == 3) {
            echo t('both options');
          }
          echo '</strong><br>';
        }
        elseif ($items['type'] == 3) {
          echo t('Airtime') . ': <strong>';
          echo $items['time'];
          echo '</strong><br>';
        }
        if ($items['type'] == 1) {
          echo t('Days in the air') . ': <strong>';
          echo $items['days'];
          echo '</strong><br>';
        }
        ?>
        <?php echo t('Words') . ': <strong>' . $items['words'] . '</strong>'; ?><br>
        <?php echo t('Amount') . ': <strong>' . $items['amount'] . ' ' . t('RUB') . '</strong>'; ?><br>
        <?php echo t('Status') . ': ';
        if ($items['status'] == 1) {
          $status = "<strong>" . t('New') . "</strong> (" . date('d.m.Y', $items['status_time']) . ')';
        }
        elseif ($items['status'] == 2) {
          $status = "<strong>" . t('Paid') . "</strong> (" . date('d.m.Y', $items['status_time']) . ')';
        }
        elseif ($items['status'] == 3) {
          $status = "<strong>" . t('During') . "</strong> (" . date('d.m.Y', $items['status_time']) . ')';
        }
        elseif ($items['status'] == 4) {
          $status = "<strong>" . t('Made') . "</strong> (" . date('d.m.Y', $items['status_time']) . ')';
        }
        elseif ($items['status'] == 0) {
          $status = "<strong>" . t('Canceled') . "</strong> (" . date('d.m.Y', $items['status_time']) . ")";
        }
        echo $status;
        ?>
      </td>
    </tr>
  </table><br>
  <table class="tabrnw">
    <tbody>
    <tr>
      <th class="rc"><?php echo t('Текст для размещения в эфире') ?></th>
      <?php
      if ($items['type'] == 1) {
        echo '<th nowrap>' . t('Контакты в объявлении') . '</th>';
      }
      elseif (($items['type'] == 2) or ($items['type'] == 3)) {
        echo '<th class=\"lc\" nowrap>' . t('Желаемая песня') . '</th>';
      }
      ?>
    </tr>
    </tbody>
    <tr>
      <td>
        <?php echo nl2br($items['contents']); ?>
      </td>
      <td>
        <?php
        if ($items['type'] == 1) {
          echo $items['contacts'];
        }
        elseif (($items['type'] == 2) or ($items['type'] == 3)) {
          echo '<p>1. ' . $items['song1'];
          echo '<p>2. ' . $items['song2'];
        }
        ?>
      </td>
    </tr>
  </table></div>