<?php

/**
 * @file
 * The user of the module rnw
 */

function rnw_order_form($form, & $form_state, $orders = NULL) {

  global $user;

  $form['customerdetails'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customer Information'),
  );

  $form['customerdetails']['name'] = array(
    '#title'         => t('Full Name'),
    '#description'   => t('Last name, first name and patronymic to determine your payment.'),
    '#type'          => 'textfield',
    '#default_value' => $user ? $user->name : '',
    '#required'      => TRUE,
  );

  $form['customerdetails']['phone'] = array(
    '#title'         => t('Phone'),
    '#description'   => t('Phone customer. The last 10 digits.'),
    '#type'          => 'textfield',
    '#size' => 12,
    '#maxlength' => 10,
    '#default_value' => $orders ? $orders['phone'] : '',
    '#required'      => TRUE,
  );

  $form['customerdetails']['mail'] = array(
    '#title'         => t('E-mail'),
    '#description'   => t('E-mail customer.'),
    '#type'          => 'textfield',
    '#default_value' => $user ? $user->mail : '',
    '#required'      => FALSE,
  );

  $form['orderdetails'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ordering Information'),
  );

  $form['orderdetails']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Order type'),
    '#default_value' => 2,
    '#options' => array(
      1 => t('announcement') . ' (' . variable_get('rnw_price_announcement', 0) . t('RUB').')',
      2 => t('felicitation'),
      3 => t('vip felicitation') . ' (' . variable_get('rnw_price_vip', 0) . t('RUB').')',
    ),
    '#required'      => TRUE,
  );

  $form['orderdetails']['dateair'] = array(
    '#title'         => t('Air date'),
    '#description'   => t('Desired air date.'),
    '#type'          => 'date',
    '#required'      => TRUE,
    '#default_value' => array(
      'day' => format_date(time() + 86400, 'custom', 'j'),
      'month' => format_date(time(), 'custom', 'n'),
      'year' => format_date(time(), 'custom', 'Y'),
    ),
  );

  $form['orderdetails']['time'] = array(
    '#title'         => t('Time of airing'),
    '#description'   => t('Desired time airing.'),
    '#type'          => 'textfield',
    '#size' => 5,
    '#maxlength' => 5,
    '#required'      => FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 3),
      ),
    ),
  );

  $form['orderdetails']['airtime'] = array(
    '#type' => 'radios',
    '#title' => t('Airtime'),
    '#default_value' => 0,
    '#options' => array(
      1 => t('daily air') . ' (' . variable_get('rnw_price_1', 0) . t('RUB').')',
      2 => t('evening air') . ' (' . variable_get('rnw_price_2', 0) . t('RUB').')',
      3 => t('both options') . ' (' . variable_get('rnw_price_3', 0) . t('RUB').')',
    ),
    '#required'      => FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 2),
      ),
    ),
  );

  $form['orderdetails']['days'] = array(
    '#title'         => t('Amount of days'),
    '#description'   => t('How many days should the announcement.'),
    '#type'          => 'textfield',
    '#default_value' => '1',
    '#required'      => TRUE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['orderdetails']['contents'] = array(
    '#title' => t('Ordering information'),
    '#type' => 'textarea',
    '#description' => t('The text of the order for placement in the air.') . 'Стандартно - ' . variable_get('rnw_order_words', 0) . ' слов от 3-х букв. Каждое слово свыше - ' . variable_get('rnw_amount_word', 0) . t('RUB'),
    '#default_value' => '',
    '#cols' => 60,
    '#rows' => 3,
    '#resizable' => TRUE,
    '#required'      => TRUE,
  );

  $form['orderdetails']['contacts'] = array(
    '#title'         => t('Contact us for order'),
    '#description'   => t('Contacts for scoring in the ad.'),
    '#type'          => 'textfield',
    '#required'      => FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['orderdetails']['song1'] = array(
    '#title'         => t('Song 1'),
    '#description'   => t('The name of the song and artist. The desired song in greeting.'),
    '#type'          => 'textfield',
    '#default_value' => '',
    '#required'      => FALSE,
    '#states' => array(
      'invisible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['orderdetails']['song2'] = array(
    '#title'         => t('Song 2'),
    '#description'   => t('Another version of the song for the congratulations. In the case of absence of the songs in the base station.'),
    '#type'          => 'textfield',
    '#default_value' => '',
    '#required'      => FALSE,
    '#states' => array(
      'invisible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'rnw/myorder'),
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function rnw_order_form_validate($form, &$form_state) {

  $airdate = $form_state['values']['dateair']['day'] . '.' . $form_state['values']['dateair']['month'] . '.' . $form_state['values']['dateair']['year'];

  if (($form_state['values']['type'] == 2) and (strtotime($airdate) < (strtotime('today') + 86400))) {
    form_set_error('dateair', t('Заказать обычное поздравление можно не ранее чем на завтра!'));
  }

  if (count(explode(' ', $form_state['values']['name'])) <> 3) {
    form_set_error('name', t('Неверно заполнено ФИО! Необходимо для оплаты заказа! Нужно вводить: Фамилия Имя Отчество.'));
  }

  if ($form_state['values']['mail']) {
    if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", trim($form_state['values']['mail']))) {
      form_set_error('mail', t('Некорректный email-адрес!'));
    }
  }

  if(!preg_match("/^[0-9]{10,10}$/", $form_state['values']['phone'])) {
    form_set_error('phone', t('Некорректный номер телефона! Посмотрите пример в комментарии к полю ввода.'));
  }

}

/**
 * Implements hook_admin_form_submit().
 */

function rnw_order_form_submit($form, & $form_state) {

  global $user;

  /*считаем длину строки*/
  $matches = array();
  preg_match_all('/(\d+|\w{3,})/u', $form_state['values']['contents'], $matches);
  $matches = implode(" ", $matches[0]);
  $words = explode(" ", $matches);
  $words = count($words);

  /*считаем сумму*/
  if ($form_state['values']['type'] == 1) {
    $price_default = variable_get('rnw_price_announcement', 0);
  }
  elseif (($form_state['values']['type'] == 2) and ($form_state['values']['airtime'] == 1)) {
    $price_default = variable_get('rnw_price_1', 0);
  }
  elseif (($form_state['values']['type'] == 2) and ($form_state['values']['airtime'] == 2)) {
    $price_default = variable_get('rnw_price_2', 0);
  }
  elseif (($form_state['values']['type'] == 2) and ($form_state['values']['airtime'] == 3)) {
    $price_default = variable_get('rnw_price_3', 0);
  }
  elseif ($form_state['values']['type'] == 3) {
    $price_default = variable_get('rnw_price_vip', 0);
  }
  else {
    $form_state['values']['airtime'] = 3;
    $price_default = variable_get('rnw_price_3', 0);
  }

  if ($words < variable_get('rnw_order_words', 50)) {
    $amount = $price_default;
  }
  else {
    $amount = (($words - variable_get('rnw_order_words', 50)) * variable_get('rnw_amount_word', 0)) + $price_default;
  }
  if ($form_state['values']['type'] == 1) {
    $amount = $amount * $form_state['values']['days'];
  }

  $airdate = $form_state['values']['dateair']['day'] . '.' . $form_state['values']['dateair']['month'] . '.' . $form_state['values']['dateair']['year'];

  $orders = array(
    'uid' => $user->uid,
    'name' => $form_state['values']['name'],
    'phone' => $form_state['values']['phone'],
    'mail' => $form_state['values']['mail'],
    'type' => $form_state['values']['type'],
    'dateair' => strtotime($airdate),
    'time' => $form_state['values']['time'],
    'airtime' => $form_state['values']['airtime'],
    'days' => $form_state['values']['days'],
    'contents' => $form_state['values']['contents'],
    'words' => $words,
    'amount' => $amount,
    'contacts' => $form_state['values']['contacts'],
    'song1' => $form_state['values']['song1'],
    'song2' => $form_state['values']['song2'],
    'created_at' => time(),
    'status_time' => time(),
  );


  drupal_write_record('rnw_orders', $orders);
  drupal_set_message(t('Order added!'));

  if ($form_state['values']['mail']) {
    $subject = t('We expect the new payment order');
    $message_b = $form_state['values']['name'] . t(', you have created a new order on the station\'s website. We will start working with your order immediately upon receipt of payment. On the methods of payment, visit the website.');
    rnw_send_mail($form_state['values']['mail'], $subject, $message_b);
  }

  drupal_goto('rnw/myorder');
}

/**
 * Implements hook_contents().
 */
function rnw_contents($display) {

  global $user;

  $query = db_select('rnw_orders', 'n')
    ->fields('n', array('id', 'uid', 'created_at', 'type', 'dateair', 'amount', 'status', 'status_time'))
    ->condition('n.uid', $user->uid)
    ->orderBy('created_at', 'DESC');

  if ($display == 'block') {
    $query->range(0, 5);
  }

  return $query->execute();

}

/**
 * Implements hook_page().
 */
function rnw_page() {

  drupal_set_title(t('My Orders'));
  $result = rnw_contents('page')->fetchAll();
  if (!$result) {
    $page_array['rnw_arguments'] = array(
      '#title'  => t('My Orders'),
      '#markup' => t('No orders available'),
    );
    return $page_array;
  }
  else {
    $page_array = theme('rnw_page', array('urls' => $result));
    return $page_array;
  }

}

/**
 * Implements hook_content().
 */
function rnw_content($orders) {

  drupal_set_title(t('Order #').$orders['id']);
  $page_array = theme('rnw_content', array('items' => $orders));

  return $page_array;

}

