<?php

/**
 * @file
 * The administrative part of the module rnw
 */

/**
 * Form builder function for module settings.
 */
function rnw_theme_settings() {

  $form['rnw_mail_alert'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail alerts for orders'),
    '#default_value' => variable_get('rnw_mail_alert', ''),
    '#required' => TRUE,
  );

  $form['rnw_order_words'] = array(
    '#type' => 'textfield',
    '#title' => t('The rate of the order of words in the text'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_order_words', 50),
    '#required' => TRUE,
  );

  $form['rnw_amount_word'] = array(
    '#type' => 'textfield',
    '#title' => t('The price in excess of 1 word'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_amount_word', 0),
    '#required' => FALSE,
  );

  $form['rnw_price_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Price exit in the daytime air'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_price_1', 0),
    '#required' => TRUE,
  );

  $form['rnw_price_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Price output on the evening air'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_price_2', 0),
    '#required' => TRUE,
  );

  $form['rnw_price_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Price exit day and night'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_price_3', 0),
    '#required' => TRUE,
  );

  $form['rnw_price_vip'] = array(
    '#type' => 'textfield',
    '#title' => t('Price exit vip'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_price_vip', 0),
    '#required' => TRUE,
  );

  $form['rnw_price_announcement'] = array(
    '#type' => 'textfield',
    '#title' => t('Price 1 exit announcement'),
    '#size' => 6,
    '#default_value' => variable_get('rnw_price_announcement', 0),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_admin_form().
 */
function rnw_admin_form($form, & $form_state, $orders = NULL) {

  $form['customerdetails'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customer Information'),
  );

  $form['customerdetails']['name'] = array(
    '#title'         => t('Full Name'),
    '#description'   => t('Last name, first name and patronymic to determine your payment.'),
    '#type'          => 'textfield',
    '#default_value' => $orders ? $orders['name'] : '',
    '#required'      => TRUE,
  );

  $form['customerdetails']['phone'] = array(
    '#title'         => t('Phone'),
    '#description'   => t('Phone customer. The last 10 digits.'),
    '#type'          => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => $orders ? $orders['phone'] : '',
    '#required'      => TRUE,
  );

  $form['customerdetails']['mail'] = array(
    '#title'         => t('E-mail'),
    '#description'   => t('E-mail customer.'),
    '#type'          => 'textfield',
    '#default_value' => $orders ? $orders['mail'] : '',
    '#required'      => FALSE,
  );

  $form['orderdetails'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ordering Information'),
  );

  $form['orderdetails']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Order type'),
    '#default_value' => $orders ? $orders['type'] : '2',
    '#options' => array(
      1 => t('announcement') . ' (' . variable_get('rnw_price_announcement', 0) . t('RUB') . ')',
      2 => t('felicitation'),
      3 => t('vip felicitation') . ' (' . variable_get('rnw_price_vip', 0) . t('RUB') . ')',
    ),
    '#required'      => TRUE,
  );

  $form['orderdetails']['dateair'] = array(
    '#title'         => t('Air date'),
    '#description'   => t('Desired air date.'),
    '#type'          => 'date',
    '#required'      => TRUE,
    '#default_value' => array(
      'day' => format_date(($orders ? $orders['dateair'] : time() + 86400), 'custom', 'j'),
      'month' => format_date(($orders ? $orders['dateair'] : time()), 'custom', 'n'),
      'year' => format_date(($orders ? $orders['dateair'] : time()), 'custom', 'Y'),
    ),
  );

  $form['orderdetails']['time'] = array(
    '#title'         => t('Time of airing'),
    '#description'   => t('Desired time airing.'),
    '#type'          => 'textfield',
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => $orders ? $orders['time'] : '',
    '#required'      => FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 3)
      ),
    ),
  );

  $form['orderdetails']['airtime'] = array(
    '#type' => 'radios',
    '#title' => t('Airtime'),
    '#default_value' => $orders ? $orders['airtime'] : '0',
    '#options' => array(
      1 => t('daily air') . ' (' . variable_get('rnw_price_1', 0) . t('RUB').')',
      2 => t('evening air') . ' (' . variable_get('rnw_price_2', 0) . t('RUB').')',
      3 => t('both options') . ' (' . variable_get('rnw_price_3', 0) . t('RUB').')',
    ),
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 2)
      ),
    ),
  );

  $form['orderdetails']['days'] = array(
    '#title'         => t('Amount of days'),
    '#description'   => t('How many days should the announcement.'),
    '#type'          => 'textfield',
    '#default_value' => $orders ? $orders['days'] : '1',
    '#required'      => TRUE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['orderdetails']['contents'] = array(
    '#title' => t('Ordering information'),
    '#type' => 'textarea',
    '#description' => t('The text of the order for placement in the air.'),
    '#default_value' => $orders ? $orders['contents'] : '',
    '#cols' => 60,
    '#rows' => 3,
    '#resizable' => TRUE,
  );

  $form['orderdetails']['contacts'] = array(
    '#title'         => t('Contact us for order'),
    '#description'   => t('Contacts for scoring in the ad.'),
    '#type'          => 'textfield',
    '#default_value' => $orders ? $orders['contacts'] : '',
    '#required'      => FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['orderdetails']['song1'] = array(
    '#title'         => t('Song 1'),
    '#description'   => t('The name of the song and artist. The desired song in greeting.'),
    '#type'          => 'textfield',
    '#default_value' => $orders ? $orders['song1'] : '',
    '#required'      => FALSE,
    '#states' => array(
      'invisible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  $form['orderdetails']['song2'] = array(
    '#title'         => t('Song 2'),
    '#description'   => t('Another version of the song for the congratulations. In the case of absence of the songs in the base station.'),
    '#type'          => 'textfield',
    '#default_value' => $orders ? $orders['song2'] : '',
    '#required'      => FALSE,
    '#states' => array(
      'invisible' => array(
        'input[name="type"]' => array('value' => 1),
      ),
    ),
  );

  if (isset($orders['id'])) {
    $form['status'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => array(
        0 => t('Canceled'),
        1 => t('New'),
        2 => t('Paid'),
        3 => t('During'),
        4 => t('Made'),
      ),
      '#default_value' => 3,
      '#description' => t('Order status when you create should be New'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $orders ? t('Save') : t('Add'),
  );
  if (!empty($orders)) {
    if (($orders['status'] == 0) or ($orders['status'] == 1)) {
      $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
      );
    }
    $form['old_status'] = array(
      '#type' => 'hidden',
      '#value' => $orders['status'],
    );
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $orders['id'],
    );
  }
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/rnw'),
  );

  return $form;
}

/**
 * Implements hook_admin_form_validate().
 */
function rnw_admin_form_validate($form, &$form_state) {

  if (count(explode(' ', $form_state['values']['name'])) <> 3) {
    form_set_error('name', t('Неверно заполнено ФИО! Необходимо для оплаты заказа! Нужно вводить: Фамилия Имя Отчество.'));
  }

  if ($form_state['values']['mail']) {
    if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", trim($form_state['values']['mail']))) {
      form_set_error('mail', t('Некорректный email-адрес!'));
    }
  }

  if(!preg_match ("/^[0-9]{10,10}$/", $form_state['values']['phone'])) {
    form_set_error ('phone', t('Некорректный номер телефона! Посмотрите пример в комментарии к полю ввода.'));
  }

}

/**
 * Implements hook_admin_form_submit().
 */
function rnw_admin_form_submit($form, & $form_state) {

  global $user;

  if ($form_state['values']['op'] == t('Delete')) {
    drupal_goto('admin/rnw/' . $form_state['values']['id'] . '/delete');
  }

  /*считаем длину строки*/
  $matches = array();
  preg_match_all('/(\d+|\w{3,})/u', $form_state['values']['contents'], $matches);
  $matches = implode(" ", $matches[0]);
  $words = explode(" ", $matches);
  $words = count($words);

  /*считаем сумму*/
  if ($form_state['values']['type'] == 1) {
    $price_default = variable_get('rnw_price_announcement', 0);
  }
  elseif (($form_state['values']['type'] == 2) and ($form_state['values']['airtime'] == 1)) {
    $price_default = variable_get('rnw_price_1', 0);
  }
  elseif (($form_state['values']['type'] == 2) and ($form_state['values']['airtime'] == 2)) {
    $price_default = variable_get('rnw_price_2', 0);
  }
  elseif (($form_state['values']['type'] == 2) and ($form_state['values']['airtime'] == 3)) {
    $price_default = variable_get('rnw_price_3', 0);
  }
  elseif ($form_state['values']['type'] == 3) {
    $price_default = variable_get('rnw_price_vip', 0);
  }
  else {
    $form_state['values']['airtime'] = 3;
    $price_default = variable_get('rnw_price_3', 0);
  }

  if ($words < variable_get('rnw_order_words', 50)) {
    $amount = $price_default;
  }
  else {
    $amount = (($words - variable_get('rnw_order_words', 50)) * variable_get('rnw_amount_word', 0)) + $price_default;
  }
  if ($form_state['values']['type'] == 1) {
    $amount = $amount * $form_state['values']['days'];
  }


  $airdate = $form_state['values']['dateair']['day'].'.'.$form_state['values']['dateair']['month'].'.'.$form_state['values']['dateair']['year'];

  if (isset($form_state['values']['id'])) {
    // save edit data
    $orders = array(
      'name' => $form_state['values']['name'],
      'phone' => $form_state['values']['phone'],
      'mail' => $form_state['values']['mail'],
      'type' => $form_state['values']['type'],
      'dateair' => strtotime($airdate),
      'time' => $form_state['values']['time'],
      'airtime' => $form_state['values']['airtime'],
      'days' => $form_state['values']['days'],
      'contents' => $form_state['values']['contents'],
      'words' => $words,
      'amount' => $amount,
      'contacts' => $form_state['values']['contacts'],
      'song1' => $form_state['values']['song1'],
      'song2' => $form_state['values']['song2'],
      'status' => $form_state['values']['status'],
      'status_time' => time(),
    );

    $orders['id'] = $form_state['values']['id'];
    drupal_write_record('rnw_orders', $orders, 'id');
    drupal_set_message(t('Order saved!'));

    if ($form_state['values']['status'] == 1) {
      $status = t('New');
    }
    elseif ($form_state['values']['status'] == 2) {
      $status = t('Paid');
    }
    elseif ($form_state['values']['status'] == 3) {
      $status = t('During');
    }
    elseif ($form_state['values']['status'] == 4) {
      $status = t('Made');
    }
    elseif ($form_state['values']['status'] == 0) {
      $status = t('Canceled');
    }
    if ($form_state['values']['status'] <> $form_state['values']['old_status']) {
      $subject = t('Changed status of the order #').$form_state['values']['id'];
      $message_b = t('Changed status of the order #').$form_state['values']['id'].'<br>'.t('Current status: ').$status;
      if ($form_state['values']['mail']) {
        rnw_send_mail($form_state['values']['mail'], $subject, $message_b);
      }
      if ($form_state['values']['status'] == 2) {
        rnw_send_mail(variable_get('rnw_mail_alert'), $subject, $message_b);
      }
    }

  }
  else {
    // add new data
    $orders = array(
      'uid' => $user->uid,
      'name' => $form_state['values']['name'],
      'phone' => $form_state['values']['phone'],
      'mail' => $form_state['values']['mail'],
      'type' => $form_state['values']['type'],
      'dateair' => strtotime($airdate),
      'time' => $form_state['values']['time'],
      'airtime' => $form_state['values']['airtime'],
      'days' => $form_state['values']['days'],
      'contents' => $form_state['values']['contents'],
      'words' => $words,
      'amount' => $amount,
      'contacts' => $form_state['values']['contacts'],
      'song1' => $form_state['values']['song1'],
      'song2' => $form_state['values']['song2'],
      'created_at' => time(),
      'status_time' => time(),
    );

    drupal_write_record('rnw_orders', $orders);
    drupal_set_message(t('Order added!'));
  }

  drupal_goto('admin/rnw');
}

/**
 * Implements hook_list().
 */
function orders_list() {
  $header = array(
    array('data' => t('Order number')),
    array('data' => t('Date')),
    array('data' => t('Full Name')),
    array('data' => t('Phone')),
    array('data' => t('Type')),
    array('data' => t('Air date')),
    array('data' => t('Amount')),
    array('data' => t('Status')),
    array('data' => t('Action')),
  );
  $orders = db_select('rnw_orders', 'n')
    ->fields('n', array('id', 'name', 'phone', 'type', 'dateair', 'created_at', 'amount', 'status', 'status_time'))
    ->execute()->fetchAll();
  $row = array();
  if ($orders) {
    foreach ($orders as $rnw) {
      $actions = array(
        l(t('просмотреть'), 'rnw/' . $rnw->id . '/items'),
        l(t('edit'), 'admin/rnw/' . $rnw->id . '/edit'),
      );
      if ($rnw->status == 1) {
        $status = "<strong>".t('New')."</strong> (" . date('d.m.Y', $rnw->status_time) . ")";
      }
      elseif ($rnw->status == 2) {
        $status = "<strong>".t('Paid')."</strong> (" . date('d.m.Y', $rnw->status_time) . ")";
      }
      elseif ($rnw->status == 3) {
        $status = "<strong>".t('During')."</strong> (" . date('d.m.Y', $rnw->status_time) . ")";
      }
      elseif ($rnw->status == 4) {
        $status = t('Made')." (" . date('d.m.Y', $rnw->status_time) . ")";
      }
      elseif ($rnw->status == 0) {
        $status = t('Canceled')." (" . date('d.m.Y', $rnw->status_time) . ")";
      }

      if ($rnw->type == 1) {
        $otype = t('announcement');
      }
      elseif ($rnw->type == 2) {
        $otype = t('felicitation');
      }
      elseif ($rnw->type == 3) {
        $otype = t('vip felicitation');
      }


      $row[] = array(
        array('data' => $rnw->id),
        array('data' => date('d.m.Y', $rnw->created_at)),
        array('data' => $rnw->name),
        array('data' => $rnw->phone),
        array('data' => $otype),
        array('data' => date('d.m.Y', $rnw->dateair)),
        array('data' => $rnw->amount . t('RUB')),
        array('data' => $status),
        array('data' => implode(' | ', $actions)),
      );
    }
  }

  return theme('table', array(
    'header' => $header,
    'rows'   => $row,
  ));

}

/**
 * Implements hook_delete().
 */
function rnw_delete($orders) {
  db_delete('rnw_orders')
    ->condition('id', $orders['id'])
    ->execute();
  drupal_set_message(t('Order deleted!'));
  drupal_goto('admin/rnw');
}

