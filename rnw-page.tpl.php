<?php
$sorder = 0;
$samount = 0;
?>
<div id="rnw-pager">
  <table>
    <tbody>
    <tr>
      <th><?php echo t('Order number') ?></th>
      <th><?php echo t('Date') ?></th>
      <th><?php echo t('Type') ?></th>
      <th><?php echo t('Air date') ?></th>
      <th><?php echo t('Amount') ?></th>
      <th><?php echo t('Status') ?></th>
      <th><?php echo t('Action') ?></th>
    </tr>
    </tbody>
    <?php foreach ($urls as $url): ?>
      <tr>
        <td><?php echo $url->id; ?></td>
        <td><?php echo date('d.m.Y', $url->created_at); ?></td>
        <td>
          <?php
          if ($url->type == 1) {
            echo t('announcement');
          }
          elseif ($url->type == 2) {
            echo t('felicitation');
          }
          elseif ($url->type == 3) {
            echo t('vip felicitation');
          }
          ?></td>
        <td>
          <?php
          if ($url->status == 0) {
            echo '<s>' . date('d.m.Y', $url->dateair) . '</s>';
          }
          else {
            echo date('d.m.Y', $url->dateair);
          }
          ?>
        </td>
        <td>
          <?php
          if ($url->status == 0) {
            echo '<s>' . $url->amount . ' ' . t('RUB') . '</s>';
          }
          else {
            echo $url->amount . ' ' . t('RUB');
            $samount = $samount + $url->amount;
            $sorder = $sorder + 1;
          }
          ?>
        </td>
        <td>
          <?php
          if ($url->status == 1) {
            $status = "<em>" . t('New') . "</em> (" . date('d.m.Y', $url->status_time) . ")";
          }
          elseif ($url->status == 2) {
            $status = "<strong>" . t('Paid') . "</strong> (" . date('d.m.Y', $url->status_time) . ")";
          }
          elseif ($url->status == 3) {
            $status = "<strong>" . t('During') . "</strong> (" . date('d.m.Y', $url->status_time) . ")";
          }
          elseif ($url->status == 4) {
            $status = t('Made') . ' (' . date('d.m.Y', $url->status_time) . ')';
          }
          elseif ($url->status == 0) {
            $status = t('Canceled') . ' (' . date('d.m.Y', $url->status_time) . ')';
          }
          echo $status;
          ?>
        </td>
        <td><a target="_blank" href="<?php echo $url->id; ?>/items">просмотреть</a></td>
      </tr>
    <?php endforeach; ?>
  </table>
  <?php echo "<br><hr>Заказов: " . $sorder . "<br>На сумму: " . $samount . " руб."; ?>
</div>
