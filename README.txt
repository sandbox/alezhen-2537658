CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

 INTRODUCTION
 ------------

 Module orders for a site radio with the ability to pay through the payment system "Qiwi".

  * For a full description of the module, visit the project page:
    https://www.drupal.org/sandbox/alezhen/2537658

  * To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/project/issues/2537658

REQUIREMENTS
------------
This module requires the following modules:

 * Views (https://drupal.org/project/views)
 *

RECOMMENDED MODULES
-------------------
 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

TROUBLESHOOTING
---------------

 * If the menu does not display, check the following:

   - Are the "Access rnw" and "Use the administration pages
     and help" permissions enabled for the appropriate roles?

   - Does html.tpl.php of your theme output the $page_bottom variable?

FAQ
---

Q: I enabled "Aggregate and compress CSS files", but admin_menu.css is still
   there. Is this normal?

A: Yes, this is the intended behavior. the administration menu module only loads
   its stylesheet as needed (i.e., on page requests by logged-on, administrative
   users).

MAINTAINERS
-----------

Current maintainers:
 * Aleksey Babanin (alezhen) - https://www.drupal.org/user/788780

This project has been sponsored by:
 * AleZhen.RU
   Visit http://www.alezhen.ru for more information.